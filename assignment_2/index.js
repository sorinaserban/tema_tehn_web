
const FIRST_NAME = "SORINA";
const LAST_NAME = "SERBAN";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
    var cacheobj= {}
    cacheobj.about=0;
    cacheobj.home=0;
    cacheobj.contact=0;
    cacheobj.pageAccessCounter = function(x)
    {
        let that = this;
        if(typeof(x) !== 'undefined')
        {
            if(x=='about' || x=='ABOUT')
                that.about=that.about+1;
            if(x=='contact' || x=='CONTACT')
                that.contact=that.contact+1;
        }
        else
            that.home=that.home+1;
        this.about=that.about;
        this.home=that.home;
    }
    cacheobj.getCache = function()
    {
        return cacheobj;
    }
    return cacheobj;   
}

let x = initCaching();
console.log(x);
x.pageAccessCounter('about');
x.pageAccessCounter('contact');
x.pageAccessCounter('ABOUT');
x.pageAccessCounter();

console.log(x);
console.log(x.getCache());

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

